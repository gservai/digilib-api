<?php

use \Mockery as Mockery;

/**
* ItemController Tests
*
* @author Gavin Servai
*/
class ItemControllerTest extends TestCase {

    private $itemController = null;

    public function setUp()
    {
        $this->itemController = Mockery::mock('App\Http\Controllers\Item\ItemController')->makePartial();
    }

    public function testCreate()
    {
        $request = Mockery::mock('\Illuminate\Http\Request');
        $request->shouldReceive('all')->andReturn(array());

        $item = Mockery::mock('alias:\Digilib\Item\Item');
        $item->shouldReceive('create')->andReturn(true);

        $response = $this->itemController->create($request);

        $this->assertTrue($response);
    }

    public function testGetItemById()
    {
        $id = 1;

        $item = Mockery::mock('alias:\Digilib\Item\Item');
        $item->shouldReceive('find')->with(1)->andReturn(true);

        $response = $this->itemController->getItemById($id);

        $this->assertTrue($response);
    }

    public function testUpdateSuccess()
    {
        $request = Mockery::mock('\Illuminate\Http\Request');
        $request->shouldReceive('all')->andReturn(array('upc' => 123456));

        $this->itemController->shouldReceive('validate')->with($request, array('upc' => 'required|exists:items'))
            ->andReturn(true);

        $item = Mockery::mock('alias:\Digilib\Item\Item');
        $item->shouldReceive('where')->with('upc', 123456)->andReturn($item);
        $item->shouldReceive('first')->andReturn(true);

        $response = $this->itemController->update($request);

        $this->assertTrue($response);
    }

    public function testUpdateFailureValidation()
    {
        $request = Mockery::mock('\Illuminate\Http\Request');
        $request->shouldReceive('all')->andReturn(array('upc' => 123456));


        $this->itemController->shouldReceive('validate')->with($request, array('upc' => 'required|exists:items'))
            ->andThrow('\RuntimeException'); // The actual exception that's thrown inherits RuntimeException

        $this->setExpectedException('\RuntimeException');

        $response = $this->itemController->update($request);
    }

    public function testAddToCollectionSuccessExistingItem()
    {
        $request = Mockery::mock('\Illuminate\Http\Request');
        $request->shouldReceive('all')->andReturn(array('upc' => 123456));

        $this->itemController->shouldReceive('validate')->with($request, array('upc' => 'required'))
            ->andReturn(true);

        $item = Mockery::mock('alias:\Digilib\Item\Item');
        $item->shouldReceive('where')->with('upc', 123456)->andReturn($item);
        $item->shouldReceive('first')->andReturn($item);

        \LucaDegasperi\OAuth2Server\Facades\Authorizer::shouldReceive('getResourceOwnerId')->andReturn('1');

        $user = Mockery::mock('alias:\App\User');

        $user->shouldReceive('find')->with('1')->andReturn($user);
        $user->shouldReceive('items')->andReturn($user);
        $item->id = 22;
        $user->shouldReceive('where')->with('item_id', 22)->andReturn($user);
        $user->shouldReceive('get')->andReturn($user);

        $user->shouldReceive('isEmpty')->andReturn(true);

        $user->shouldReceive('items')->andReturn($user);
        $user->shouldReceive('sync')->with([22], false)->andReturn(true);

        $user->items = [$item];


        $response = $this->itemController->addToCollection($request);

        $this->assertEquals($response, [$item]);
    }

    public function testAddToCollectionSuccessNewItem()
    {
        $request = Mockery::mock('\Illuminate\Http\Request');
        $request->shouldReceive('all')->andReturn(array('upc' => 123456));

        $this->itemController->shouldReceive('validate')->with($request, array('upc' => 'required'))
            ->andReturn(true);

        $item = Mockery::mock('alias:\Digilib\Item\Item');
        $item->shouldReceive('where')->with('upc', 123456)->andReturn($item);
        $item->shouldReceive('first')->andReturn(null);

        $item->shouldReceive('create')->andReturn($item);

        \LucaDegasperi\OAuth2Server\Facades\Authorizer::shouldReceive('getResourceOwnerId')->andReturn('1');

        $user = Mockery::mock('alias:\App\User');

        $user->shouldReceive('find')->with('1')->andReturn($user);
        $user->shouldReceive('items')->andReturn($user);
        $item->id = 22;
        $user->shouldReceive('where')->with('item_id', 22)->andReturn($user);
        $user->shouldReceive('get')->andReturn($user);

        $user->shouldReceive('isEmpty')->andReturn(true);

        $user->shouldReceive('items')->andReturn($user);
        $user->shouldReceive('sync')->with([22], false)->andReturn(true);

        $user->items = [$item];


        $response = $this->itemController->addToCollection($request);

        $this->assertEquals($response, [$item]);
    }

    public function testAddToCollectionAbortExistingItem()
    {
        $request = Mockery::mock('\Illuminate\Http\Request');
        $request->shouldReceive('all')->andReturn(array('upc' => 123456));

        $this->itemController->shouldReceive('validate')->with($request, array('upc' => 'required'))
            ->andReturn(true);

        $item = Mockery::mock('alias:\Digilib\Item\Item');
        $item->shouldReceive('where')->with('upc', 123456)->andReturn($item);
        $item->shouldReceive('first')->andReturn($item);

        \LucaDegasperi\OAuth2Server\Facades\Authorizer::shouldReceive('getResourceOwnerId')->andReturn('1');

        $user = Mockery::mock('alias:\App\User');

        $user->shouldReceive('find')->with('1')->andReturn($user);
        $user->shouldReceive('items')->andReturn($user);
        $item->id = 22;
        $user->shouldReceive('where')->with('item_id', 22)->andReturn($user);
        $user->shouldReceive('get')->andReturn($user);

        $user->shouldReceive('isEmpty')->andThrow('\RuntimeException'); // Instead of global php abort function, which is complicated to mock up
        $this->setExpectedException('\RuntimeException');

        $response = $this->itemController->addToCollection($request);
    }

    public function testAddToCollectionAbortNewItem()
    {
        $request = Mockery::mock('\Illuminate\Http\Request');
        $request->shouldReceive('all')->andReturn(array('upc' => 123456));

        $this->itemController->shouldReceive('validate')->with($request, array('upc' => 'required'))
            ->andReturn(true);

        $item = Mockery::mock('alias:\Digilib\Item\Item');
        $item->shouldReceive('where')->with('upc', 123456)->andReturn($item);
        $item->shouldReceive('first')->andReturn(null);

        $item->shouldReceive('create')->andReturn($item);

        \LucaDegasperi\OAuth2Server\Facades\Authorizer::shouldReceive('getResourceOwnerId')->andReturn('1');

        $user = Mockery::mock('alias:\App\User');

        $user->shouldReceive('find')->with('1')->andReturn($user);
        $user->shouldReceive('items')->andReturn($user);
        $item->id = 22;
        $user->shouldReceive('where')->with('item_id', 22)->andReturn($user);
        $user->shouldReceive('get')->andReturn($user);

        $user->shouldReceive('isEmpty')->andThrow('\RuntimeException'); // Instead of global php abort function, which is complicated to mock up
        $this->setExpectedException('\RuntimeException');
        
        $response = $this->itemController->addToCollection($request);

    }

    public function testRemoveFromCollectionSuccess()
    {
        $request = Mockery::mock('\Illuminate\Http\Request');
        $request->shouldReceive('all')->andReturn(array('upc' => 123456));

        $this->itemController->shouldReceive('validate')->with($request, array('upc' => 'required|exists:items'))
            ->andReturn(true);

        $item = Mockery::mock('alias:\Digilib\Item\Item');
        $item->id = 22;
        $item->shouldReceive('where')->with('upc', 123456)->andReturn($item);
        $item->shouldReceive('first')->andReturn($item);

        \LucaDegasperi\OAuth2Server\Facades\Authorizer::shouldReceive('getResourceOwnerId')->andReturn('1');
        $user = Mockery::mock('alias:\App\User');
        $user->shouldReceive('find')->with('1')->andReturn($user);

        $user->shouldReceive('items')->andReturn($user);
        $user->shouldReceive('detach')->with('22')->andReturn(true);

        $user->items = [$user];

        $response = $this->itemController->removeFromCollection($request);

        $this->assertEquals($response, [$user]);
    }

    public function testRemoveFromCollectionFailureValidation()
    {
        $request = Mockery::mock('\Illuminate\Http\Request');
        $request->shouldReceive('all')->andReturn(array('upc' => 123456));

        $this->itemController->shouldReceive('validate')->with($request, array('upc' => 'required|exists:items'))
            ->andThrow('\RuntimeException');

        $this->setExpectedException('\RuntimeException');

        $response = $this->itemController->removeFromCollection($request);

    }

    public function testAddToWishListSuccessExistingItem()
    {
        $request = Mockery::mock('\Illuminate\Http\Request');
        $request->shouldReceive('all')->andReturn(array('upc' => 123456));

        $this->itemController->shouldReceive('validate')->with($request, array('upc' => 'required'))
            ->andReturn(true);

        $item = Mockery::mock('alias:\Digilib\Item\Item');
        $item->shouldReceive('where')->with('upc', 123456)->andReturn($item);
        $item->shouldReceive('first')->andReturn($item);

        \LucaDegasperi\OAuth2Server\Facades\Authorizer::shouldReceive('getResourceOwnerId')->andReturn('1');

        $user = Mockery::mock('alias:\App\User');

        $user->shouldReceive('find')->with('1')->andReturn($user);
        $user->shouldReceive('wishlistItems')->andReturn($user);
        $item->id = 22;
        $user->shouldReceive('where')->with('item_id', 22)->andReturn($user);
        $user->shouldReceive('get')->andReturn($user);

        $user->shouldReceive('isEmpty')->andReturn(true);

        $user->shouldReceive('wishlistItems')->andReturn($user);
        $user->shouldReceive('sync')->with([22], false)->andReturn(true);

        $user->wishlistItems = [$item];


        $response = $this->itemController->addToWishlist($request);

        $this->assertEquals($response, [$item]);
    }

    public function testAddToWishListSuccessNewItem()
    {
        $request = Mockery::mock('\Illuminate\Http\Request');
        $request->shouldReceive('all')->andReturn(array('upc' => 123456));

        $this->itemController->shouldReceive('validate')->with($request, array('upc' => 'required'))
            ->andReturn(true);

        $item = Mockery::mock('alias:\Digilib\Item\Item');
        $item->shouldReceive('where')->with('upc', 123456)->andReturn($item);
        $item->shouldReceive('first')->andReturn(null);

        $item->shouldReceive('create')->andReturn($item);

        \LucaDegasperi\OAuth2Server\Facades\Authorizer::shouldReceive('getResourceOwnerId')->andReturn('1');

        $user = Mockery::mock('alias:\App\User');

        $user->shouldReceive('find')->with('1')->andReturn($user);
        $user->shouldReceive('wishlistItems')->andReturn($user);
        $item->id = 22;
        $user->shouldReceive('where')->with('item_id', 22)->andReturn($user);
        $user->shouldReceive('get')->andReturn($user);

        $user->shouldReceive('isEmpty')->andReturn(true);

        $user->shouldReceive('wishlistItems')->andReturn($user);
        $user->shouldReceive('sync')->with([22], false)->andReturn(true);

        $user->wishlistItems = [$item];


        $response = $this->itemController->addToWishList($request);

        $this->assertEquals($response, [$item]);
    }

    public function testAddToWishListAbortExistingItem()
    {
        $request = Mockery::mock('\Illuminate\Http\Request');
        $request->shouldReceive('all')->andReturn(array('upc' => 123456));

        $this->itemController->shouldReceive('validate')->with($request, array('upc' => 'required'))
            ->andReturn(true);

        $item = Mockery::mock('alias:\Digilib\Item\Item');
        $item->shouldReceive('where')->with('upc', 123456)->andReturn($item);
        $item->shouldReceive('first')->andReturn($item);

        \LucaDegasperi\OAuth2Server\Facades\Authorizer::shouldReceive('getResourceOwnerId')->andReturn('1');

        $user = Mockery::mock('alias:\App\User');

        $user->shouldReceive('find')->with('1')->andReturn($user);
        $user->shouldReceive('wishlistItems')->andReturn($user);
        $item->id = 22;
        $user->shouldReceive('where')->with('item_id', 22)->andReturn($user);
        $user->shouldReceive('get')->andReturn($user);

        $user->shouldReceive('isEmpty')->andThrow('\RuntimeException'); // Instead of global php abort function, which is complicated to mock up
        $this->setExpectedException('\RuntimeException');

        $response = $this->itemController->addToWishlist($request);
    }

    public function testRemoveFromWishlistSuccess()
    {
        $request = Mockery::mock('\Illuminate\Http\Request');
        $request->shouldReceive('all')->andReturn(array('upc' => 123456));

        $this->itemController->shouldReceive('validate')->with($request, array('upc' => 'required|exists:items'))
            ->andReturn(true);

        $item = Mockery::mock('alias:\Digilib\Item\Item');
        $item->id = 22;
        $item->shouldReceive('where')->with('upc', 123456)->andReturn($item);
        $item->shouldReceive('first')->andReturn($item);

        \LucaDegasperi\OAuth2Server\Facades\Authorizer::shouldReceive('getResourceOwnerId')->andReturn('1');
        $user = Mockery::mock('alias:\App\User');
        $user->shouldReceive('find')->with('1')->andReturn($user);

        $user->shouldReceive('wishlistItems')->andReturn($user);
        $user->shouldReceive('detach')->with('22')->andReturn(true);

        $user->wishlistItems = [$user];

        $response = $this->itemController->removeFromWishlist($request);

        $this->assertEquals($response, [$user]);
    }

    public function testRemoveFromWishlistFailureValidation()
    {
        $request = Mockery::mock('\Illuminate\Http\Request');
        $request->shouldReceive('all')->andReturn(array('upc' => 123456));

        $this->itemController->shouldReceive('validate')->with($request, array('upc' => 'required|exists:items'))
            ->andThrow('\RuntimeException');

        $this->setExpectedException('\RuntimeException');

        $response = $this->itemController->removeFromWishlist($request);

    }

    public function testGetItemsUserNameExistingUserSuccess()
    {
        $request = Mockery::mock('\Illuminate\Http\Request');
        $request->shouldReceive('has')->with('username')->andReturn(true);
        $request->shouldReceive('input')->with('username')->andReturn('gavin');

        $user = Mockery::mock('alias:\App\User');
        $user->shouldReceive('where')->with('username', 'gavin')->andReturn($user);
        $user->shouldReceive('first')->andReturn($user);

        $user->items = true;

        $response = $this->itemController->getItems($request);
        $this->assertTrue($response);
    }

    public function testGetItemsUserNameNewUserSuccess()
    {
        $request = Mockery::mock('\Illuminate\Http\Request');
        $request->shouldReceive('has')->with('username')->andReturn(true);
        $request->shouldReceive('input')->with('username')->andReturn('gavin');

        $user = Mockery::mock('alias:\App\User');
        $user->shouldReceive('where')->with('username', 'gavin')->andReturn($user);
        $user->shouldReceive('first')->andReturn(null);

        \LucaDegasperi\OAuth2Server\Facades\Authorizer::shouldReceive('getResourceOwnerId')->andReturn('1');
        $user->shouldReceive('find')->with('1')->andReturn($user);
        $user->items = true;

        $response = $this->itemController->getItems($request);
        $this->assertTrue($response);
    }

    public function testGetItemsWithoutUserNameExistingUserSuccess()
    {
        $request = Mockery::mock('\Illuminate\Http\Request');
        $request->shouldReceive('has')->with('username')->andReturn(false);

        $user = Mockery::mock('alias:\App\User');

        \LucaDegasperi\OAuth2Server\Facades\Authorizer::shouldReceive('getResourceOwnerId')->andReturn('1');
        $user->shouldReceive('find')->with('1')->andReturn($user);
        $user->items = true;

        $response = $this->itemController->getItems($request);
        $this->assertTrue($response);
    }

    public function testGetWishlistItemsUserNameExistingUserSuccess()
    {
        $request = Mockery::mock('\Illuminate\Http\Request');
        $request->shouldReceive('has')->with('username')->andReturn(true);
        $request->shouldReceive('input')->with('username')->andReturn('gavin');

        $user = Mockery::mock('alias:\App\User');
        $user->shouldReceive('where')->with('username', 'gavin')->andReturn($user);
        $user->shouldReceive('first')->andReturn($user);

        $user->wishlistItems = true;

        $response = $this->itemController->getWishlistItems($request);
        $this->assertTrue($response);
    }

    public function testGetWishlistItemsUserNameNewUserSuccess()
    {
        $request = Mockery::mock('\Illuminate\Http\Request');
        $request->shouldReceive('has')->with('username')->andReturn(true);
        $request->shouldReceive('input')->with('username')->andReturn('gavin');

        $user = Mockery::mock('alias:\App\User');
        $user->shouldReceive('where')->with('username', 'gavin')->andReturn($user);
        $user->shouldReceive('first')->andReturn(null);

        \LucaDegasperi\OAuth2Server\Facades\Authorizer::shouldReceive('getResourceOwnerId')->andReturn('1');
        $user->shouldReceive('find')->with('1')->andReturn($user);
        $user->wishlistItems = true;

        $response = $this->itemController->getWishlistItems($request);
        $this->assertTrue($response);
    }

    public function testGetWishlistItemsWithoutUserNameExistingUserSuccess()
    {
        $request = Mockery::mock('\Illuminate\Http\Request');
        $request->shouldReceive('has')->with('username')->andReturn(false);

        $user = Mockery::mock('alias:\App\User');

        \LucaDegasperi\OAuth2Server\Facades\Authorizer::shouldReceive('getResourceOwnerId')->andReturn('1');
        $user->shouldReceive('find')->with('1')->andReturn($user);
        $user->wishlistItems = true;

        $response = $this->itemController->getWishlistItems($request);
        $this->assertTrue($response);
    }

    public function testLookupUPCSuccess()
    {
        $request = Mockery::mock('\Illuminate\Http\Request');
        $this->itemController->shouldReceive('validate')->with($request, array('upc' => 'required'))
            ->andReturn(true);

        $request->shouldReceive('input')->with('upc')->andReturn('123456');
        $lookupAmazon = Mockery::mock('overload:\Digilib\Item\LookupAmazon');

        $lookupAmazon->shouldReceive('findByUPC')->with('123456')->andReturn(true);

        $response = $this->itemController->lookupUPC($request);
        $this->assertTrue($response);
    }

    public function testLookupUPCFailureValidation()
    {
        $request = Mockery::mock('\Illuminate\Http\Request');
        $this->itemController->shouldReceive('validate')->with($request, array('upc' => 'required'))
            ->andThrow('\RuntimeException');

        $this->setExpectedException('\RuntimeException');

        $response = $this->itemController->lookupUPC($request);
    }



}