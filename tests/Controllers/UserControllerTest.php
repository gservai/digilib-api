<?php

use \Mockery as Mockery;

/**
* UserController Tests
*
* @author Gavin Servai
*/
class UserControllerTest extends TestCase {

    private $userController = null;

    public function setUp()
    {
        $this->userController = Mockery::mock('App\Http\Controllers\User\UserController')->makePartial();
    }

    public function testFavoriteUserSuccess()
    {
        $request = Mockery::mock('\Illuminate\Http\Request');
        $this->userController->shouldReceive('validate')->with($request, array('user_id' => 'required|exists:users,id'))
            ->andReturn(true);

        $request->shouldReceive('all')->andReturn(['user_id' => '53']);

        $user = Mockery::mock('alias:\App\User');
        \LucaDegasperi\OAuth2Server\Facades\Authorizer::shouldReceive('getResourceOwnerId')->andReturn('53');
        $user->shouldReceive('find')->with('53')->andReturn($user);

        $user->shouldReceive('favoriteUsers')->andReturn($user);
        $user->shouldReceive('sync')->with(['53']);

        $user->favoriteUsers = true;

        $response = $this->userController->favoriteUser($request);
    }

    public function testFavoriteFailureValidation()
    {
        $request = Mockery::mock('\Illuminate\Http\Request');
        $this->userController->shouldReceive('validate')->with($request, array('user_id' => 'required|exists:users,id'))
            ->andThrow('\RuntimeException');

        $this->setExpectedException('\RuntimeException');

        $response = $this->userController->favoriteUser($request);
    }

    public function testGetFavoritesSuccess()
    {
        $request = Mockery::mock('\Illuminate\Http\Request');
        $user = Mockery::mock('alias:\App\User');
        \LucaDegasperi\OAuth2Server\Facades\Authorizer::shouldReceive('getResourceOwnerId')->andReturn('53');
        $user->shouldReceive('find')->with('53')->andReturn($user);

        $user->favoriteUsers = true;

        $response = $this->userController->getFavorites($request);
        $this->assertTrue($response);
    }

    public function testContactUserSuccess()
    {
        $request = Mockery::mock('\Illuminate\Http\Request');
        $this->userController->shouldReceive('validate')->with($request, array('username' => 'required|exists:users,username'))
            ->andReturn(true);

        $request->shouldReceive('all')->andReturn(['username' => 'gavin']);
        $user = Mockery::mock('alias:\App\User');
        $user->id = '53';
        \LucaDegasperi\OAuth2Server\Facades\Authorizer::shouldReceive('getResourceOwnerId')->andReturn('53');
        $user->shouldReceive('find')->with('53')->andReturn($user);

        $user->shouldReceive('where')->with('username', 'gavin')->andReturn($user);
        $user->shouldReceive('first')->andReturn($user);

        $user->shouldReceive('contactedUsers')->andReturn($user);
        $user->shouldReceive('sync')->with(['53'], false)->andReturn(true);

        $user->username = $user->email = $user->name = 'gavins';

        $mail = Mockery::mock('alias:\Mail');
        $mail->shouldReceive('send');

        $user->contactedUsers = true;

        $response = $this->userController->contactUser($request);
        $this->assertTrue($response);

    }

    public function testContactUserFailureValidation()
    {
        $request = Mockery::mock('\Illuminate\Http\Request');
        $this->userController->shouldReceive('validate')->with($request, array('username' => 'required|exists:users,username'))
            ->andThrow('\RuntimeException');

        $this->setExpectedException('\RuntimeException');

        $response = $this->userController->contactUser($request);
    }

    public function testGetContactedSuccess()
    {
        $request = Mockery::mock('\Illuminate\Http\Request');
        $user = Mockery::mock('alias:\App\User');
        $user->id = '53';
        $user->contactedUsers = true;
        \LucaDegasperi\OAuth2Server\Facades\Authorizer::shouldReceive('getResourceOwnerId')->andReturn('53');
        $user->shouldReceive('find')->with('53')->andReturn($user);

        $response = $this->userController->getContacted($request);
        $this->assertTrue($response);

    }

    public function testReceivedContactsSuccess()
    {
        $request = Mockery::mock('\Illuminate\Http\Request');
        $user = Mockery::mock('alias:\App\User');
        $user->id = '53';
        $user->requestsUsers = true;
        \LucaDegasperi\OAuth2Server\Facades\Authorizer::shouldReceive('getResourceOwnerId')->andReturn('53');
        $user->shouldReceive('find')->with('53')->andReturn($user);

        $response = $this->userController->getReceivedContacts($request);
        $this->assertTrue($response);

    }

    public function testAcceptContactSuccess()
    {
        $request = Mockery::mock('\Illuminate\Http\Request');
        $this->userController->shouldReceive('validate')->with($request, [
            'id' => 'required|exists:user_contact_user,id',
            'message' => 'required'
        ])->andReturn(true);

        $request->shouldReceive('all')->andReturn(['id' => '53', 'message' => 'This is a test message']);
        $contact = Mockery::mock('alias:\Digilib\Contact\Contact');
        $contact->shouldReceive('find')->with('53')->andReturn($contact);
        $contact->shouldReceive('accept')->with('This is a test message')->andReturn(true);

        $response = $this->userController->acceptContact($request);
        $this->assertTrue($response);
    }

    public function testAcceptContactFailureValidation()
    {
        $request = Mockery::mock('\Illuminate\Http\Request');
        $this->userController->shouldReceive('validate')->with($request, [
            'id' => 'required|exists:user_contact_user,id',
            'message' => 'required'
        ])->andThrow('\RuntimeException');

        $this->setExpectedException('\RuntimeException');

        $response = $this->userController->acceptContact($request);
    }

    public function testPostGeoLocationSuccess()
    {
        $request = Mockery::mock('\Illuminate\Http\Request');
        $this->userController->shouldReceive('validate')->with($request, [
            'latitude' => 'required',
            'longitude' => 'required'
        ])->andReturn(true);

        $request->shouldReceive('input')->with('latitude')->andReturn(40);
        $request->shouldReceive('input')->with('longitude')->andReturn(50);

        $user = Mockery::mock('alias:\App\User');
        $user->id = '53';
        \LucaDegasperi\OAuth2Server\Facades\Authorizer::shouldReceive('getResourceOwnerId')->andReturn('53');
        $user->shouldReceive('find')->with('53')->andReturn($user);

        $user->shouldReceive('save')->andReturn(true);

        $response = $this->userController->postGeolocation($request);
        $this->assertEquals($response, $user);
    }

    public function testPostGeoLocationFailureValidation()
    {
        $request = Mockery::mock('\Illuminate\Http\Request');
        $this->userController->shouldReceive('validate')->with($request, [
            'latitude' => 'required',
            'longitude' => 'required'
        ])->andThrow('\RuntimeException');

        $this->setExpectedException('\RuntimeException');

        $response = $this->userController->postGeolocation($request);
    }

    public function testGetNearbyUsersWithLocationSuccess()
    {
        $request = Mockery::mock('\Illuminate\Http\Request');

        $user = Mockery::mock('alias:\App\User');
        $user->id = '53';
        \LucaDegasperi\OAuth2Server\Facades\Authorizer::shouldReceive('getResourceOwnerId')->andReturn('53');
        $user->shouldReceive('find')->with('53')->andReturn($user);

        $user->longitude = 40;
        $user->latitude = 50;

        $user->shouldReceive('where')->with('longitude', '>', 38)->andReturn($user);
        $user->shouldReceive('where')->with('longitude', '<', 42)->andReturn($user);
        $user->shouldReceive('where')->with('latitude', '>', 48)->andReturn($user);
        $user->shouldReceive('where')->with('latitude', '<', 52)->andReturn($user);
        $user->shouldReceive('get')->andReturn(true);

        $response = $this->userController->getNearbyUsers($request);
        $this->assertTrue($response);

    }

    public function testGetNearbyUsersWithoutLongitudeSuccess()
    {
        $request = Mockery::mock('\Illuminate\Http\Request');

        $user = Mockery::mock('alias:\App\User');
        $user->id = '53';
        \LucaDegasperi\OAuth2Server\Facades\Authorizer::shouldReceive('getResourceOwnerId')->andReturn('53');
        $user->shouldReceive('find')->with('53')->andReturn($user);

        $user->longitude = null;
        $user->latitude = 50;

        $user->shouldReceive('all')->andReturn(true);

        $response = $this->userController->getNearbyUsers($request);
        $this->assertTrue($response);
    }

    public function testGetNearbyUsersWithoutLatitudeSuccess()
    {
        $request = Mockery::mock('\Illuminate\Http\Request');

        $user = Mockery::mock('alias:\App\User');
        $user->id = '53';
        \LucaDegasperi\OAuth2Server\Facades\Authorizer::shouldReceive('getResourceOwnerId')->andReturn('53');
        $user->shouldReceive('find')->with('53')->andReturn($user);

        $user->longitude = 40;
        $user->latitude = null;

        $user->shouldReceive('all')->andReturn(true);

        $response = $this->userController->getNearbyUsers($request);
        $this->assertTrue($response);
    }

    

}