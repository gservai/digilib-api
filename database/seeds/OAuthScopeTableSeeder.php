<?php

use Illuminate\Database\Seeder;

/**
 * Class OAuthScopeTableSeeder
 *
 * @author Gavin Servai
 */
class OAuthScopeTableSeeder extends Seeder {
    public function run()
    {
        DB::table('oauth_scopes')->delete();

        // Create basic Scope
        ApiScope::createScope('basic', 'Default scope. Read only access to public data.'); // Must clarify requirements to see if this is even a use case

        // Create user Scope
        ApiScope::createScope('user', 'Able to run all relevant actions as a registered user');
        
    }
}