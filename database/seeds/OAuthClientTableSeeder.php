<?php

use Illuminate\Database\Seeder;

/**
 * Class OAuthClientTableSeeder
 *
 * @author Gavin Servai
 */
class OAuthClientTableSeeder extends Seeder {
    public function run()
    {
        DB::table('oauth_clients')->delete();

        // Create Initial API Client
        ApiClient::createClient(
            'digilib-client',
            'b0e742235d500365c1b26ef59c3c159f',
            '$2y$10$Xcn1pP3D1Q07dqQcKo4v1Ov7MBdCAVhybsJEXc0CCKkT9qaPIpUuK'
        );
    }
}