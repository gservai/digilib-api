<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAmazonData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('items', function ($table) {
            $table->string('title');
            $table->string('publisher');
            $table->string('releaseDate');
            $table->string('platform');
            $table->string('platformGroup');
            $table->string('imageURL');
            $table->string('ESRB');
            $table->string('description');
            $table->string('upc');
            $table->string('genre');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
