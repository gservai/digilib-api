<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
* Schema for items, collections, wishlist
*
* @author Gavin Servai
*/
class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // All items scanned onto Digilib
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('ISBN', 50);
            $table->timestamps();
        });

        // All tags added for items
        Schema::create('tags', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('text');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });

        // Users item collections
        Schema::create('user_item', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('item_id')->unsigned();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('item_id')->references('id')->on('items');
        });

        // Item tags per user collection
        Schema::create('user_item_tag', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('user_item_id')->unsigned();
            $table->integer('tag_id')->unsigned();
            $table->timestamps();

            $table->foreign('user_item_id')->references('id')->on('user_item');
            $table->foreign('tag_id')->references('id')->on('tags');
        });

        // Users wishlist (This is essentially the same as the users collection schema-wise)
        Schema::create('user_wishlist_item', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('item_id')->unsigned();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('item_id')->references('id')->on('items');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_wishlist_item');
        Schema::drop('user_item_tag');
        Schema::drop('user_item');
        Schema::drop('tags');
        Schema::drop('items');
    }
}
