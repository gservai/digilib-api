<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
* Creates schema for user contacting, as well as favoriting
*
* @author Gavin Servai
*/
class CreateUserInteraction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Users favoriting other users
        Schema::create('user_favorite_user', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('from_id')->unsigned();
            $table->integer('to_id')->unsigned();
            $table->timestamps();

            $table->foreign('from_id')->references('id')->on('users');
            $table->foreign('to_id')->references('id')->on('users');
        });

        // Users requesting to contact other users
        Schema::create('user_contact_user', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('from_id')->unsigned();
            $table->integer('to_id')->unsigned();
            $table->timestamps();

            $table->foreign('from_id')->references('id')->on('users');
            $table->foreign('to_id')->references('id')->on('users');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_contact_user');
        Schema::drop('user_favorite_user');
    }
}
