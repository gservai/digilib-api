<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);

// To register a new user
Route::post('auth/register', 'Auth\AuthController@postRegister');
    
// To obtain an access token
Route::post('oauth/access_token', function() {
    return Response::json(Authorizer::issueAccessToken());
});

Route::group(['middleware' => 'oauth:user', 'prefix' => 'item'], function() {
    // Add new item to collection
    Route::post('collection', 'Item\ItemController@addToCollection');

    // Remove item from collection
    Route::post('delete', 'Item\ItemController@removeFromCollection');

    // Get items in collection
    Route::get('collection', 'Item\ItemController@getItems');

    // Add item to wishlist
    Route::post('wishlist', 'Item\ItemController@addToWishlist');

    // To delete an item from a wishlist
    Route::post('wishdelete', 'Item\ItemController@removeFromWishList');

    // Retrieve items in a users wishlist
    Route::get('wishlist', 'Item\ItemController@getWishlistItems');

    // Get item by id
    Route::get('{id}', 'Item\ItemController@getItemById');
});

Route::group(['middleware' => 'oauth:user', 'prefix' => 'user'], function() {

    // Favorite a user
    Route::post('favorite', 'User\UserController@favoriteUser');

    // Retrieve a users favorites list
    Route::get('favorite', 'User\UserController@getFavorites');

    // Retrieve a users contacted list
    Route::get('contact', 'User\UserController@getContacted');

    // Retrieve a list of contact requests that the user received
    Route::get('received', 'User\UserController@getReceivedContacts');

    // To add a new item to a collection
    Route::post('contact', 'User\UserController@contactUser');

    // To store the geolocation of the user
    Route::post('geolocation', 'User\UserController@postGeolocation');

    // To get a list of nearby users
    Route::get('nearby', 'User\UserController@getNearbyUsers');

    // Accept a contact request
    Route::post('accept', 'User\UserController@acceptContact');
});


// Find an item by UPC
Route::get('amazon/upc', ['middleware' => 'oauth:user', 'uses' => 'Item\ItemController@lookupUPC']);


    


