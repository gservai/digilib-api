<?php

namespace App\Http\Controllers\Item;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;
use \Illuminate\Http\Request as Request;
use \Digilib\Item\Item as Item;
use \Digilib\Item\LookupAmazon as LookupAmazon;

/**
* Logic for managing Items
*
* @author Gavin Servai
*/
class ItemController extends Controller
{

    /**
     * Add a new item
     *
     * @author Gavin Servai
     */
    public function create(Request $request)
    {

        $data = $request->all();

        return Item::create(
            $data
        );
    }

    /**
    * Retreives item information, given the primary key for that item
    *
    * @author Gavin Servai
    */
    public function getItemById($id)
    {
        $item = Item::find($id);

        return $item;
    }

    /**
    * Update an existing item
    *
    * @author Gavin Servai
    */
    public function update(Request $request)
    {
        $this->validate($request, [
            'upc' => 'required|exists:items',
        ]);

        $data = $request->all();

        // Logic to update pertinent data here, and return the Item instance
        return Item::where('upc', $data['upc'])->first();
    }

    /**
    * Add an item to the users collection. Globally adds the item if an item with that ISBN code does not already exist
    *
    * @author Gavin Servai
    */
    public function addToCollection(Request $request)
    {
        $this->validate($request, [
            'upc' => 'required',
        ]);

        $data = $request->all();

        $item = Item::where('upc', $data['upc'])->first();

        if (is_null($item)) {
            $item = Item::create($data);
        }

        $userId = Authorizer::getResourceOwnerId();
        $user = User::find($userId);

        $alreadyExists = null;
        $alreadyExists = $user->items()->where('item_id', $item->id)->get();

        if (!$alreadyExists->isEmpty()) {
            abort(500,'Error, item already in collection');
        }

        $user->items()->sync([$item->id], false);

        return $user->items;
    }

    /**
    * Remove an item from the users collection
    *
    * @author Gavin Servai
    */
    public function removeFromCollection(Request $request)
    {
        $this->validate($request, [
            'upc' => 'required|exists:items',
        ]);

        $data = $request->all();

        $item = Item::where('upc', $data['upc'])->first();

        $userId = Authorizer::getResourceOwnerId();
        $user = User::find($userId);

        $user->items()->detach($item->id);

        return $user->items;
    }

    /**
    * Add an item to the users wishlist. Globally adds the item if an item with that ISBN code does not already exist
    *
    * @author Gavin Servai
    */
    public function addToWishlist(Request $request)
    {
        $this->validate($request, [
            'upc' => 'required',
        ]);

        $data = $request->all();

        $item = Item::where('upc', $data['upc'])->first();

        if (is_null($item)) {
            $item = Item::create($data);
        }

        $userId = Authorizer::getResourceOwnerId();
        $user = User::find($userId);

        $alreadyExists = null;
        $alreadyExists = $user->wishlistItems()->where('item_id', $item->id)->get();

        if (!$alreadyExists->isEmpty()) {
            abort(500,'Error, item already in wishlist');
        }

        $user->wishlistItems()->sync([$item->id], false);

        return $user->wishlistItems;
    }

    /**
    * Remove an item from the users collection
    *
    * @author Gavin Servai
    */
    public function removeFromWishlist(Request $request)
    {
        $this->validate($request, [
            'upc' => 'required|exists:items',
        ]);

        $data = $request->all();

        $item = Item::where('upc', $data['upc'])->first();

        $userId = Authorizer::getResourceOwnerId();
        $user = User::find($userId);

        $user->wishlistItems()->detach($item->id);

        return $user->wishlistItems;
    }

    /**
    * Retreives the users collection.
    * If a 'username' is provided, it will return the collection of the appropriate corresponding user
    *
    * @author Gavin Servai
    */
    public function getItems(Request $request)
    {

        if ($request->has('username')) {
            $user = User::where('username', $request->input('username'))->first();
            if (!is_null($user)) {
                return $user->items; 
            }
        }

        $userId = Authorizer::getResourceOwnerId();
        $user = User::find($userId);

        return $user->items;
    }

    /**
    * Retreives the users wishlist
    *
    * @author Gavin Servai
    */
    public function getWishlistItems(Request $request)
    {
        if ($request->has('username')) {
            $user = User::where('username', $request->input('username'))->first();
            if (!is_null($user)) {
                return $user->wishlistItems; 
            }
        }

        $userId = Authorizer::getResourceOwnerId();
        $user = User::find($userId);

        return $user->wishlistItems;
    }

    /**
    * Lookup a product by UPC on the Amazon Product Advertising API
    *
    * @author Gavin Servai
    */
    public function lookupUPC(Request $request)
    {
        $this->validate($request, [
            'upc' => 'required',
        ]);

        $upc = $request->input('upc');
        $lookup = new LookupAmazon();
        $product = $lookup->findByUPC($upc);

        return $product;
    }

}
