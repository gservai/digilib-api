<?php

namespace App\Http\Controllers\User;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;
use \Illuminate\Http\Request as Request;
use \Digilib\Contact\Contact as Contact;

/**
* Logic for managing User Interaction
*
* @author Gavin Servai
*/
class UserController extends Controller
{

    /**
    * Favorite a User
    *
    * @author Gavin Servai
    */
    public function favoriteUser(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required|exists:users,id',
        ]);

        $data = $request->all();

        $userId = Authorizer::getResourceOwnerId();
        $user = User::find($userId);

        $user->favoriteUsers()->sync([$data['user_id']]);

        // Logic to update pertinent data here, and return the Item instance
        return $user->favoriteUsers;
    }

    /**
    * Get the Users Favorites
    *
    * @author Gavin Servai
    */
    public function getFavorites(Request $request)
    {
        $userId = Authorizer::getResourceOwnerId();
        $user = User::find($userId);

        return $user->favoriteUsers;
    }

    /**
    * Contact a User
    *
    * @author Gavin Servai
    */
    public function contactUser(Request $request)
    {   
        $this->validate($request, [
            'username' => 'required|exists:users,username',
        ]);

        $data = $request->all();

        $userId = Authorizer::getResourceOwnerId();
        $user = User::find($userId);

        $contactee = User::where('username', $data['username'])->first();
        $user->contactedUsers()->sync([$contactee->id], false);

        // Send a message to the user who is contacted
        \Mail::send('emails.contact_received', ['username' => $user->username], function($m) use ($contactee) {
            $m->from('donotreply@digilib.com', 'Digilib Team');
            $m->to($contactee->email, $contactee->name)->subject('Digilib - Contact Request Received!');
        });

        // Logic to update pertinent data here, and return the Item instance
        return $user->contactedUsers;
    }

    /**
    * Get the Users sent Requests
    *
    * @author Gavin Servai
    */
    public function getContacted(Request $request)
    {
        $userId = Authorizer::getResourceOwnerId();
        $user = User::find($userId);

        return $user->contactedUsers;
    }

    /**
    * Get the Users received Requests
    *
    * @author Gavin Servai
    */
    public function getReceivedContacts(Request $request)
    {
        $userId = Authorizer::getResourceOwnerId();
        $user = User::find($userId);

        return $user->requestsUsers;
    }

    /**
    * Accept a trade request
    *
    * @author Gavin Servai
    */
    public function acceptContact(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|exists:user_contact_user,id',
            'message' => 'required'
        ]);
        $data = $request->all();
        $contactRequest = Contact::find($data['id']);
        
        return $contactRequest->accept($data['message']);
    }

    /**
    * Store longitude and latitude
    *
    * @author Gavin Servai
    */
    public function postGeolocation(Request $request)
    {
        $this->validate($request, [
            'latitude' => 'required',
            'longitude' => 'required'
        ]);

        $userId = Authorizer::getResourceOwnerId();
        $user = User::find($userId);

        $user->latitude = $request->input('latitude');
        $user->longitude = $request->input('longitude');

        $user->save();

        return $user;
    }

    /**
    * Retrieve a list of nearby users
    *
    * @author Gavin Servai
    */
    public function getNearbyUsers(Request $request)
    {
        $userId = Authorizer::getResourceOwnerId();
        $user = User::find($userId);

        $longitude = $user->longitude;
        $latitude = $user->latitude;

        if (empty($longitude) || empty($latitude)) {
            return User::all();
        }

        $nearbyUsers = User::where('longitude', '>', $longitude - 2)
                            ->where('longitude', '<', $longitude + 2)
                            ->where('latitude', '>', $latitude - 2)
                            ->where('latitude', '<', $latitude + 2)
                            ->get();

        return $nearbyUsers;
    }

    
}
