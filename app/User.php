<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password', 'username', 'latitude', 'longitude'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
    * Items in the users collection
    *
    * @author Gavin Servai
    */
    public function items()
    {
        return $this->belongsToMany('\Digilib\Item\Item', 'user_item');
    }

    /**
    * Items in the users wishlist
    *
    * @author Gavin Servai
    */
    public function wishlistItems()
    {
        return $this->belongsToMany('\Digilib\Item\Item', 'user_wishlist_item');
    }

    /**
    * Users favorited by the current user
    *
    * @author Gavin Servai
    */
    public function favoriteUsers()
    {
        return $this->belongsToMany('\App\User', 'user_favorite_user', 'from_id', 'to_id');
    }

    /**
    * Users contacted by the current user
    *
    * @author Gavin Servai
    */
    public function contactedUsers()
    {
        return $this->belongsToMany('\App\User', 'user_contact_user', 'from_id', 'to_id')->withPivot('id', 'accepted', 'message');
    }

    /**
    * Requested received by the current user
    *
    * @author Gavin Servai
    */
    public function requestsUsers()
    {
        return $this->belongsToMany('\App\User', 'user_contact_user', 'to_id', 'from_id')->withPivot('id', 'accepted', 'message');
    }

}
