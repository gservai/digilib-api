<?php namespace Digilib\Api;

use Illuminate\Support\ServiceProvider;

/**
* Used to provide singleton instances of the API 
*
* @author Gavin Servai
*/
class ApiServiceProvider extends ServiceProvider
{
    protected $defer = true;

    public function register()
    {
        $this->app->singleton('digilib.apiclient', function($app)
        {
            return new ApiClient();
        });

        $this->app->singleton('digilib.apiscope', function($app)
        {
            return new ApiScope();
        });
    }

    public function provides()
    {
        return ['digilib.apiclient', 'digilib.apiscope'];
    }
}