<?php namespace Digilib\Api;


/**
* Contains methods to manage OAuth clients
*
* @author Gavin Servai
*/
class ApiClient
{
    /**
    * Creates a Client
    *
    * @param String $clientName   Name of the client to be added
    * @param String $clientId     Desired client id
    * @param String $clientSecret Desired client secret
    */
    public function createClient($clientName, $clientId = null, $clientSecret = null)
    {
        if (is_null($clientId)) {
            $clientId = $this->generateClientKey($clientName);   
        }
        
        $existing = OAuthClient::find($clientId);
        if (!is_null($existing)) {
            // Client already exists
            return array();
        }

        if (is_null($clientSecret)) {
            $clientSecret = $this->generateClientSecret($clientId);
        }

        $data = array(
            'id'        => $clientId,
            'secret'    => $clientSecret,
            'name'      => $clientName
        );

        OAuthClient::create($data);

        return $data;
    }

    /**
    * Generates a key for the Client
    *
    * @param String $clientName Name of the client
    */
    protected function generateClientKey($clientName)
    {
        $key = \Hash::make($clientName);
        return $key;
    }

    /**
    * Generates a secret for the Client
    *
    * @param String $clientKey Key of the client
    */
    protected function generateClientSecret($clientKey)
    {
        $secret = \Hash::make($clientKey . time());
        return $secret;
    }


}