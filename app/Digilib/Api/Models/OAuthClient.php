<?php namespace Digilib\Api;

/**
* Exists so that clients can be created
*
* @author Gavin Servai
*/
class OAuthClient extends \Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'oauth_clients';

    protected $fillable = array('id', 'secret', 'name');

}