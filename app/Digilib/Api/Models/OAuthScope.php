<?php namespace Digilib\Api;

/**
* Exists so that Scopes can be created
*
* @author Gavin Servai
*/
class OAuthScope extends \Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'oauth_scopes';

    protected $fillable = array('scope', 'name', 'description');

}