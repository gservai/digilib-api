<?php namespace Digilib\Api;

use Illuminate\Support\Facades\Auth;

/**
* Used for Verification of the Password Grant OAuth Flow
*
* @author Gavin Servai
*/
class Verifier
{
  public function verify($username, $password)
  {
      $credentials = [
        'email'    => $username,
        'password' => $password,
      ];

      if (Auth::once($credentials)) {
          return Auth::user()->id;
      }

      return false;
  }
}