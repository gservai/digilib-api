<?php namespace Digilib\Api\Facades;

use Illuminate\Support\Facades\Facade;

class ApiScope extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'digilib.apiscope';
    }
}