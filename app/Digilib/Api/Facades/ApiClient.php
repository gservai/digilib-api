<?php namespace Digilib\Api\Facades;

use Illuminate\Support\Facades\Facade;

class ApiClient extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'digilib.apiclient';
    }
}