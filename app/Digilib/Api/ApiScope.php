<?php namespace Digilib\Api;


/**
* Contains methods to manage OAuth scopes
*
* @author Gavin Servai
*/
class ApiScope
{
    /**
    * Creates a Scope
    *
    * @param String $scopeName        Name of desired scope
    * @param String $scopeDescription Description of desired scope
    */
    public function createScope($scopeName, $scopeDescription='')
    {
        $existing = OAuthScope::whereScope($scopeName);

        if (!is_null($existing) && count($existing) > 1) {
            // Scope already exists
            return array();
        }

        $data = array(
            'id'          => $scopeName,
            'description'   => $scopeDescription
        );

        OAuthScope::create($data);

        return $data;
    }

}