<?php namespace Digilib\Contact;

use \App\User as User;

/**
* Exists so that users can contact each other
*
* @author Gavin Servai
*/
class Contact extends \Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_contact_user';

    protected $fillable = array('id', 'accepted', 'message');

    /**
    * Accepts a contact request
    *
    * @param String $message The user should specify some details that they can be contacted at
    */
    public function accept($acceptmessage)
    {   


        $this->accepted = 1;
        $this->message = $acceptmessage;
        $this->save();

        // Fire off an email, to let the user know that they've received a contact request
        $fromUser = User::find($this->from_id);
        $toUser = User::find($this->to_id);

        \Mail::send('emails.contact_accept', ['username' => $toUser->username, 'acceptmessage' => $this->message ], function($m) use ($fromUser) {
            $m->from('donotreply@digilib.com', 'Digilib Team');
            $m->to($fromUser->email, $fromUser->name)->subject('Digilib - Contact Request Accepted!');
        });

        return $toUser->requestsUsers;
    }    

}