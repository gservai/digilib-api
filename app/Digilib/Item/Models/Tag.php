<?php namespace Digilib\Item;

/**
* Exists so that clients can be created
*
* @author Gavin Servai
*/
class Tag extends \Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tags';

    protected $fillable = array('id', 'user_id', 'text');

}