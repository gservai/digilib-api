<?php namespace Digilib\Item;

/**
* Exists so that clients can be created
*
* @author Gavin Servai
*/
class Item extends \Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'items';

    protected $fillable = array('id', 'ISBN', 'title', 'publisher', 'releaseDate', 'platform', 'platformGroup', 'imageUrl', 'ESRB', 'description', 'upc', 'genre');    

}