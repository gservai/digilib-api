<?php namespace Digilib\Item;

use \ApaiIO\Configuration\GenericConfiguration;
use \ApaiIO\Operations\Lookup;
use \ApaiIO\ApaiIO;


/**
* Searches for an item on Amazon via the Product Advertising API
*
* @author Gavin Servai
*/
class LookupAmazon {

    private $apaiIO = null;

    function __construct()
    {
        $conf = new GenericConfiguration();
        $conf
            ->setCountry('com')
            ->setAccessKey('AKIAIOE5LU5NLGTLDX5A')
            ->setSecretKey('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')
            ->setAssociateTag('chord03-20');
            //->setResponseTransformer('\ApaiIO\ResponseTransformer\XmlToDomDocument');


        $this->apaiIO = new ApaiIO($conf);
    }

    /**
    * Searches for an item based on its UPC
    *
    * @author Gavin Servai
    */
    public function findByUPC($upc)
    {
        $data = array();

        $lookup = new Lookup();
        $lookup->setItemId($upc);
        $lookup->setIdType('UPC');
        $lookup->setResponseGroup(array('Large'));
        
        $formattedResponse = $this->apaiIO->runOperation($lookup);

        $fileContents = $formattedResponse;
        $fileContents = str_replace(array("\n", "\r", "\t"), '', $fileContents);
        $fileContents = trim(str_replace('"', "'", $fileContents));
        $simpleXml = simplexml_load_string($fileContents);
        $json = json_encode($simpleXml);

        $productData = json_decode($json, TRUE);
        $productData = $productData['Items'];
        $json = json_encode($productData);
        
        if (!array_key_exists('Item', $productData)) {
            abort( 500, 'No results bro');
        }

        if (array_key_exists('ASIN', $productData['Item'])) {
            $itemAttributes = $productData['Item']['ItemAttributes'];
            $data['imageUrl'] = $productData['Item']['MediumImage']['URL'];


        } else {
            $itemAttributes = $productData['Item'][0]['ItemAttributes'];
            $data['imageUrl'] = $productData['Item'][0]['MediumImage']['URL'];
        }

        if (array_key_exists('Title', $itemAttributes)) 
            $data['title'] = $itemAttributes['Title'];
        if (array_key_exists('Publisher', $itemAttributes)) 
            $data['publisher'] = $itemAttributes['Publisher'];
        if (array_key_exists('ReleaseDate', $itemAttributes)) 
            $data['releaseDate'] = $itemAttributes['ReleaseDate'];
        if (array_key_exists('Platform', $itemAttributes)) 
            $data['platform'] = $itemAttributes['Platform'];
        if (array_key_exists('ProductGroup', $itemAttributes)) 
            $data['platformGroup'] = $itemAttributes['ProductGroup'];
        if (array_key_exists('ESRBAgeRating', $itemAttributes)) 
            $data['ESRB'] = $itemAttributes['ESRBAgeRating'];

        if (array_key_exists('Feature', $itemAttributes)) {
            if (!empty($itemAttributes['Feature']))
                $data['description'] = $itemAttributes['Feature'][0];
        }

        if (array_key_exists('UPC', $itemAttributes))
            $data['upc'] = $itemAttributes['UPC'];

        if (array_key_exists('Genre', $itemAttributes))
            $data['genre'] = $itemAttributes['Genre'];
        
        return json_encode($data);
    }
}