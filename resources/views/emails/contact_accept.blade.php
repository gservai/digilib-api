<!DOCTYPE html>
<html>
    <body>
        The contact request that you sent to {{ $username }}  has been accepted! <br>

        {{ $username }} replied with:
        {{ $acceptmessage }}

    </body>
</html>
