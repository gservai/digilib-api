<!DOCTYPE html>
<html>
    <body>
        You've received a contact request from {{ $username }}.

        Open up the DigiLib app to accept it!

    </body>
</html>
